% The base Java grammar and grammar overrides
include "Java.Grm"

% XOTcl grammar addons
include "XOTclAddons.Grm"

include "JavaCommentOverrides.Grm"
#pragma -comment

% Main translation rule 
function main
    replace [program] 
		P [program]
    by
		P 
end function
