% yava2tcl project
% Imports, packages, etc.
% Mykhaylo Sorochan, January 2009
% msorc@bigmir.net, macroexpand.org

% Copyright 2009 Mykhaylo Sorochan
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%    Redistributions of source code must retain the above copyright notice,
%    this list of conditions and the following disclaimer.
%    Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
%
%    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
%    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
%    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
%    AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
%    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.


function transformImpEx
    replace [program]
        P [program]
    by
        P [addNamespace] [addXOTclImport] [addXOTclImportIfNoPH] [transformImports]
end function

function addNamespace
    replace * [package_declaration]
        Cmts [repeat comment_NL]
        PkgHead [opt package_header]
        ImpDecl [repeat import_declaration]
        TD [repeat type_declaration]
    deconstruct PkgHead
        _ [repeat annotation]
        'package PkgName [package_name] ';
    deconstruct PkgName
        Ref [reference]
    construct TclPkgName [repeat tcl_id]
        _ [pkgRef2TclId Ref]
    deconstruct TclPkgName
        Id [id] Refs [repeat tcl_id_part]
    construct TclRef [tcl_id]
         ':: Id Refs
    by
        Cmts
        PkgHead
        ImpDecl
        'namespace 'eval TclRef '{
        TD
        '}
end function


function addXOTclImport
    replace * [package_header]
        Ann [repeat annotation]
        'package PkgName [package_name] ';
    deconstruct PkgName
        Ref [reference]
    construct TclPkgName [repeat tcl_id]
        _ [pkgRef2TclId Ref]
    deconstruct TclPkgName
        Id [id] Refs [repeat tcl_id_part]
    construct TclRef [tcl_id]
        ':: Id Refs
    by
    %   Ann
        'package 'provide TclRef
        'package 'require 'XOTcl
        'namespace 'import '::xotcl::*
end function

function addXOTclImportIfNoPH
    replace * [package_declaration]
        _ [repeat comment_NL]
        Imps [repeat import_declaration]
        NT [opt namespace_or_type_declaration]
    construct OHead [package_header]
        'package 'require 'XOTcl
        'namespace 'import '::xotcl::*
    by
        OHead
        Imps
        NT
end function


function transformImports
    replace * [repeat import_declaration]
        ImpDecl [repeat import_declaration]
    by
        ImpDecl [transformImport2PackageRequire]
end function

rule transformImport2PackageRequire
    replace [import_declaration]
        'import ImpName [imported_name] ';
    by
        'package 'require ImpName [transformImportedName]
end rule

function transformImportedName
    replace [imported_name]
        Ref [reference] DotStar [opt dot_star]
    construct TclPkgName [repeat tcl_id]
        _ [pkgRef2TclId Ref]
    deconstruct TclPkgName
        Id [id] Refs [repeat tcl_id_part]
    construct TclRef [tcl_id]
         ':: Id Refs
    construct TclImpName [imported_name]
        TclRef
    by
        TclImpName [addStarTail DotStar]
end function

function addStarTail DotStar [opt dot_star]
    deconstruct DotStar
        '. '*
    replace [imported_name]
        TclId [tcl_id]
    by
        TclId '::*
end function
