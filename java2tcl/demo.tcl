#!/usr/bin/env tclsh

set java_dir [file join . java]
set tcl_dir [file join . tcl]
set java2tcl txl

set platform [lindex $tcl_platform(os) 0]
if {$platform == "windows"} { set java2tcl [file join . java2tcl.exe] }

proc translate_java {} {
    global java_dir tcl_dir java2tcl

    puts "Converting Java sources..."

    foreach file  [glob -nocomplain -directory $java_dir *]  {
	if [file isdirectory $file] {
	    continue
	}

	set java_file [file tail $file]
	set tcl_file [file join $tcl_dir [regsub {.java} $java_file .tcl]]

	puts "\n$java_file"

	if {[catch {exec $java2tcl $file -o $tcl_file} err]} {
	    puts $err
	}
    }
}

proc test_converted_classes {} {
    global java_dir tcl_dir java2tcl

    puts "Launching converted XOTcl classes..."

    foreach file  [glob -nocomplain -directory $tcl_dir *]  {
	if [file isdirectory $file] {
	    continue
	}

	set tcl_file [file tail $file]
	set class_name [regsub {.tcl} $tcl_file {}]

	puts "\n########## $class_name ##########"
	source $file
	$class_name main
    }

}

translate_java
test_converted_classes


