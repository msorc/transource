#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Website(object):
    """ generated source for Website

    """
    name = ""
    url = ""
    description = ""
    site_count = 0

    def __init__(self):
        Website.site_count += 1
	Website.yo = 4
	self.site_count = 9

    def print2(self):
        print self.name + " at " + self.url + " is " + self.description
        print "Number of class instances: ",  Website.site_count, self.site_count

    @classmethod
    def main(cls, args):
        w = Website()
        w.name = "Jorika"
        w.url = "http://jorika.edu"
        w.description = "Really cool!"
        w.print2()
	w.site_count = 91
        w2 = Website()
        w.name = "Foggi 4"
        w.url = "http://foggi4youevery.fa"
        w.description = "Foggi file!"
        w.print2()

if __name__ == '__main__':
    import sys
    Website.main(sys.argv)

