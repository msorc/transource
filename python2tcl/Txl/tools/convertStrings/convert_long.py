#!/bin/env python
inp = open('in.str', 'r')
istring = eval(inp.read())
inp.close()

ostring = ''

for x in istring.splitlines():
  ostring += "{0}\n".format(x.strip())

ostring = "a=\"" + ostring + "\""

outp = open('out.str', 'w')
outp.write(ostring)
outp.close()
