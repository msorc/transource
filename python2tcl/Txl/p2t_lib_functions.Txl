% python2tcl project
% library functions
% Mykhaylo Sorochan, 2009
% msorc@bigmir.net

% Copyright 2009 Mykhaylo Sorochan
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%    Redistributions of source code must retain the above copyright notice,
%    this list of conditions and the following disclaimer.
%    Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
%
%    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
%    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
%    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
%    AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
%    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.

% split suit into [repeat stmt_or_newline] when suite is [repeat stmt_or_newline]
function splitSuiteIntoRepeatStmtOrNewline Suite [suite]
    replace [repeat stmt_or_newline]
        _ [repeat stmt_or_newline]
    deconstruct Suite
        _ [indent] _ [endofline]
        Stmts [repeat stmt_or_newline+]
        _ [dedent]
    by
        Stmts
end function

% split suit into [repeat stmt_or_newline] when suite is [simple_stmt]
function splitSuiteIntoRepeatStmtOrNewline2 Suite [suite]
    replace [repeat stmt_or_newline]
        _ [repeat stmt_or_newline]
    deconstruct Suite
        Stmt [simple_stmt] EndOfLine [endofline]
    by
        Stmt EndOfLine
end function

% python power to tcl_id
function convertPower2TclId Power [power]
    replace [repeat tcl_id]
        TclIds [repeat tcl_id]
    construct PowerIds [repeat id]
        _ [^ Power]
    deconstruct PowerIds
        FirstId [id] RestId [repeat id]
    construct TclIdHead [tcl_id_head]
        FirstId
    construct TclIdPart [repeat tcl_id_part]
        _ [addTclIdPart each RestId]
    construct NewTclId [tcl_id]
        TclIdHead TclIdPart
    by
        TclIds [. NewTclId]
end function

% add '::id part to tcl_id
function addTclIdPart Id [id]
    replace [repeat tcl_id_part]
        Parts [repeat tcl_id_part]
    construct Part [tcl_id_part]
        ':: Id
    by
        Parts [. Part]
end function


% convert python function/method parameters (x, y=4, z) to tcl's {x {y 4} z}
function convertPyParams2TclParams Parameters [parameters]
    replace [repeat tcl_proc_param]
        TPP [repeat tcl_proc_param]
    construct Params [repeat fpdef_test]
        _ [^ Parameters]
    construct TclParams [repeat tcl_proc_param]
        _ [transformParamsSelector each Params] % add transforming of '* and '**
    by
        TPP [. TclParams]
end function

function transformParamsSelector P [fpdef_test]
    replace [repeat tcl_proc_param]
        TPP [repeat tcl_proc_param]
    construct NewParam [repeat tcl_proc_param]
        _ [convertSingleParam P] [convertDefaultParam P]
    by
        TPP [. NewParam]
end function

% (x,y) -> {x y}
function convertSingleParam P [fpdef_test]
    replace [repeat tcl_proc_param]
        _ [repeat tcl_proc_param]
    deconstruct P
        EOFs [repeat endofline] Id [id]
    by
        Id
end function

% (x=5) -> {{x 5}}
function convertDefaultParam P [fpdef_test]
    replace [repeat tcl_proc_param]
        _ [repeat tcl_proc_param]
    deconstruct P
        EOFs [repeat endofline] Id [id] '= Test [test]
    deconstruct Test
        _ [repeat endofline] Or [or_test] IT [opt if_test]
    by
        '{ Id  Or IT '}
end function


