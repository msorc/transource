% python2tcl project
% class related transformations
% Mykhaylo Sorochan, 2009
% msorc@bigmir.net

% Copyright 2009 Mykhaylo Sorochan
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%    Redistributions of source code must retain the above copyright notice,
%    this list of conditions and the following disclaimer.
%    Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
%
%    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
%    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
%    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
%    AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
%    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.

%% class transforms
function classTransformations
    replace [program]
        P [program]
    by
        P
    % transform class definition -> XOTcl class and metaclass (if needed), method definitions
          [transformClassDefinition]
end function


% replace python class definitions with XOTcl's, defintions of classes and functions are changed
rule transformClassDefinition
    replace [repeat stmt_or_newline]
        ClassDef [classdef] Stmts [repeat stmt_or_newline]
    deconstruct ClassDef
        'class ClassName [id] ClassParentsDef [opt class_parents_def] ': Suite [suite]
    % get class attributes
    construct Attrs [repeat small_stmt]
        _ [fetchAttrsFromClass1 Suite] [fetchAttrsFromClass2 Suite]
    by
        % class longstring converted to comments
        _ [addClassComments Suite]
        % add meta class if number of attributes > 0
          [addXMetaClass ClassName Attrs]
        % add class if no meta is needed
          [addClassNoMeta ClassName ClassParentsDef Attrs]
        % meta class needed attrs>0
          [addClassWithMeta ClassName ClassParentsDef Attrs]
        % transform method definitions
          [transformClassMethodDefinitions ClassName Suite]
          [. Stmts]
end rule

function addClassComments Suite [suite]
    replace [repeat stmt_or_newline]
        Stmts [repeat stmt_or_newline]
    construct SuiteStmts [repeat stmt_or_newline]
        _ [splitSuiteIntoRepeatStmtOrNewline Suite]
    deconstruct SuiteStmts
        S1 [stmt_or_newline] Rest [repeat stmt_or_newline]
    deconstruct * [repeat comment] S1
        _ [repeat comment]
    by
        S1
end function



% extract class attributes if suite is [repeat stmt_or_newline]
function fetchAttrsFromClass1 Suite [suite]
    deconstruct Suite
        _ [indent] _ [endofline] Stmts [repeat stmt_or_newline] _ [dedent]
    construct Attrs [repeat small_stmt]
        _ [fetchAttrsFromStmtsRun Stmts]
    replace [repeat small_stmt]
        ExprStmts [repeat small_stmt]
    by
        ExprStmts [. Attrs]
end function

% extract class attributes if suite is [simple_stmt]
function fetchAttrsFromClass2 Suite [suite]
    deconstruct Suite
        Stmt [simple_stmt] _ [endofline]
    deconstruct Stmt
        _ [id] _ [repeat is_expr]
    construct Attrs [repeat small_stmt]
        _ [^ Stmt]
    replace [repeat small_stmt]
        ExprStmts [repeat small_stmt]
    by
        ExprStmts [. Attrs]
end function

% map through all statements and add attribute if it is found
function fetchAttrsFromStmtsRun Stmts [repeat stmt_or_newline]
    deconstruct Stmts
        Stmt1 [stmt_or_newline] StmtsRest [repeat stmt_or_newline]
    replace [repeat small_stmt]
        Attrs [repeat small_stmt]
    construct AttrNew [repeat small_stmt]
        _ [addClassAttributeToSequence Stmt1]
    by
        Attrs [. AttrNew] [fetchAttrsFromStmtsRun StmtsRest]
end function

% add attribute in Stmt (if it is there)  to the replaced sequence of attributes
function addClassAttributeToSequence Stmt [stmt_or_newline]
    deconstruct Stmt
        ExprStmt [small_stmt] _ [endofline]
    deconstruct ExprStmt
        _ [id] _ [repeat is_expr]
    replace [repeat small_stmt]
        Attrs [repeat small_stmt]
    by
        Attrs [. ExprStmt]
end function


% add XOTcl metaclass if number of attributes > 0
function addXMetaClass Name [id] Attrs [repeat small_stmt]
    deconstruct * Attrs
        _ [expr_stmt]
    replace [repeat stmt_or_newline]
        Stmt [repeat stmt_or_newline]
    % XOTcl metaclass template
    construct XMetaClassTmp [stmt_or_newline]
        'Class 'create Name [_ 'metapapa] '-superclass '{ 'Class '} '-slots '{ Attrs '}
    construct XMetaClass [stmt_or_newline]
        XMetaClassTmp [transformProperty]
                      [transformDefaultAttributes]
                      [transformDefaultAttributesList]
%                       [transformDefaultAttributesTouple]
%                       [transformPyList2TclList]
%                       [transformPyTouple2TclList]
                      [transformRestAttributes2Init]
    by
        Stmt [. XMetaClass]
end function

% add XOTcl class without metaclass
function addClassNoMeta Name [id] ClassParentsDef [opt class_parents_def] Attrs [repeat small_stmt]
    deconstruct not * Attrs
        _ [expr_stmt]
    replace [repeat stmt_or_newline]
        Stmt [repeat stmt_or_newline]
    construct Parents [repeat tcl_id]
        _ [convertParentsToTclId ClassParentsDef]
    construct Class [stmt_or_newline]
        'Class 'create Name '-superclass '{ 'Class Parents '}
    by
        Stmt [. Class]
end function

% add XOTcl class with metaclass
function addClassWithMeta Name [id] ClassParentsDef [opt class_parents_def] Attrs [repeat small_stmt]
    deconstruct * Attrs
        _ [expr_stmt]
    replace [repeat stmt_or_newline]
        Stmt [repeat stmt_or_newline]
    construct Parents [repeat tcl_id]
        _ [convertParentsToTclId ClassParentsDef]
    construct Class [stmt_or_newline]
        Name [_ 'metapapa] 'create Name '-superclass '{ 'Class Parents '}
    by
        Stmt [. Class]
end function

% convert python class parents to XOTcl parents A.B.C -> A::B::C
function convertParentsToTclId ClassParentsDef [opt class_parents_def]
    construct Powers [repeat power]
        _ [^ ClassParentsDef]
    replace [repeat tcl_id]
        TclIds [repeat tcl_id]
    construct Parents [repeat tcl_id]
        _ [convertPower2TclId each Powers]
    by
        TclIds [. Parents]
end function

%% methods
% transform methods definition
function transformClassMethodDefinitions ClassName [id] Suite [suite]
    replace [repeat stmt_or_newline]
        Stmts [repeat stmt_or_newline]
    construct Compounds [repeat compound_stmt]
        _ [^ Suite]
    construct Methods [repeat stmt_or_newline]
        _ [transformMethodDefiniton ClassName each Compounds] [transformClassMethodDefiniton ClassName each Compounds]
    by
        Stmts [. Methods]
end function

% transform @classmethod definiton
function transformClassMethodDefiniton ClassName [id] Compound [compound_stmt]
    deconstruct Compound
        '@ 'classmethod _ [newline]
        'def MethodName [id] Parameters [parameters] ': Suite [suite]
    replace [repeat stmt_or_newline]
        Stmts [repeat stmt_or_newline]
    construct Params [repeat tcl_proc_param]
        _ [convertPyParams2TclParams Parameters]
    construct Method [stmt_or_newline]
        ClassName 'proc MethodName '{ Params '} '{ Suite '}
    by
        Stmts [. Method]
end function

% transform method definiton
function transformMethodDefiniton ClassName [id] Compound [compound_stmt]
    deconstruct Compound
        'def MethodName [id] Parameters [parameters] ': Suite [suite]
    replace [repeat stmt_or_newline]
        Stmts [repeat stmt_or_newline]
    construct Params [repeat tcl_proc_param]
        _ [convertPyParams2TclParams Parameters]
    construct Method [stmt_or_newline]
        ClassName 'instproc MethodName [renameInitMethod] '{ Params '} '{ Suite '}
    by
        Stmts [addDestroyMethod ClassName MethodName] [. Method]
end function

% rename __init__ -> init
function renameInitMethod
    replace [id]
        '__init__
    by
        'init
end function

% add destroy method if there is a __del__ method in the class
function addDestroyMethod ClassName [id] Name [id]
    deconstruct Name
        '__del__
    replace [repeat stmt_or_newline]
        Stmts [repeat stmt_or_newline]

    construct T1 [tcl_expr]
        'my '__del__
    construct T2 [tcl_cmd]
        'next

    construct DestroyMethod [stmt_or_newline]
        ClassName 'instproc 'destroy '{} '{
        T1 T2
        '}
    by
        Stmts [. DestroyMethod]
end function


%% attribute transformations

% attr = value class attr transform
rule transformDefaultAttributes
    replace [small_stmt]
        Name [id] '= Literal [literal] _ [repeat endofline]
    by
        'Attribute Name '-default Literal
end rule


% attr = list class attr transform
rule transformDefaultAttributesList
    replace [small_stmt]
        Name [id] '= '[ Elements [list test] '] _ [repeat endofline]
    by
        'Attribute Name '-default '[ Elements ']
end rule


% attr = [test] class attr transform
rule transformRestAttributes2Init
    replace [small_stmt]
        Name [id] '= Test [test] _ [repeat endofline]
    by
        'Attribute Name '-initcmd '{ Test '}
end rule


%% python properties transform
% fix that
rule transformProperty
    replace [small_stmt]
        SmallStmt [small_stmt]
    deconstruct SmallStmt
        Name [id] '= 'property '( Args [arglist] ') _ [repeat endofline]
    by
        'Attribute Name '-default '0924
end rule

