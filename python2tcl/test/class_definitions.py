# Simple, no class methods and class attributes
class Simple:
  def f(self):
    return "hello world"


# Class with class attributes
class Attributed:
  i = 4
  b = func1(97)
  def f(self):
    return "hello world"


# Class with class attributes and class method
class AttrUndClMethods:
  i = 4

  def f(self):
    return "hello world"

  @classmethod
  def supaF(self):
	if b>5:
	  print "no"
    return "classmethod"


# Multiple inheritance with //_metaclass//
class MultiInher(A1, B1.Module.Class4):
  i = 4

  def f(self):
    return "hello world"


# Single inheritance without //_metaclass//
class SingleHerit(A1):
  def f(self):
    return "hello world"

# init method
class Initted:
  d = [1,3,67]
  def __init__(self, b, c=5, e):
    self.a = b
    
  def __del__(self):
    print "destroy"