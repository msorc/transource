if a < 5:
    print a

if a < 5:
    print "yes"
else:
    print "no" 

if b < 5:
    print "yes"
elif b == 65:
    print "65"

if b < 5:
    print "yes"
elif b == 65:
    print "65"
else:
    print "no"

if b < 5:
    print "yes"
elif b == 65:
    print "65"
elif b == 67:
    print "67"
else:
    print "no"

if __name__ == '__main__':
  print "main if"