Demo mode:
Launch test.sh(test.bat)

Single file mode:
txl JavaSourceFile.java
JavaSourceFile.java should be in the same dir as Txl with .txl sources

Make sure input files have line feeds corresponding to your platform.

Acknowledgements:
Vsevolod Solovyov, Andy Svetlov for help with Python language. v1.1

----------------------------------------------------------------------
Mykhaylo Sorochan, msorc@bigmir.net
