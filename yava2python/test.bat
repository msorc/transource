
txl .\java\ArithmeticDemo.java -o tmp.py
echo "**************************"
echo "ArithmeticDemo.java"
echo "**************************"
python tmp.py

txl .\java\ArrayDemo.java -o tmp.py
echo "**************************"
echo "ArrayDemo.java"
echo "**************************"
python tmp.py

txl .\java\BlockDemo.java -o tmp.py
echo "**************************"
echo "BlockDemo.java"
echo "**************************"
python tmp.py

txl .\java\ComparisonDemo.java -o tmp.py
echo "**************************"
echo "ComparisonDemo.java"
echo "**************************"
python tmp.py

txl .\java\DoWhileDemo.java -o tmp.py
echo "**************************"
echo "DoWhileDemo.java"
echo "**************************"
python tmp.py

txl .\java\ExceptionFoo.java -o tmp.py
echo "**************************"
echo "ExceptionFoo.java"
echo "**************************"
python tmp.py

txl .\java\FahrToCelsius.java -o tmp.py
echo "**************************"
echo "FahrToCelsius.java"
echo "**************************"
python tmp.py

txl .\java\ForDemo.java -o tmp.py
echo "**************************"
echo "ForDemo.java"
echo "**************************"
python tmp.py

txl .\java\IfElseDemo.java -o tmp.py
echo "**************************"
echo "IfElseDemo.java"
echo "**************************"
python tmp.py

txl .\java\PrePostDemo.java -o tmp.py
echo "**************************"
echo "PrePostDemo.java"
echo "**************************"
python tmp.py

txl .\java\SwitchDemo2.java -o tmp.py
echo "**************************"
echo "SwitchDemo2.java"
echo "**************************"
python tmp.py

txl .\java\SwitchDemo.java -o tmp.py
echo "**************************"
echo "SwitchDemo.java"
echo "**************************"
python tmp.py

txl .\java\UnaryDemo.java -o tmp.py
echo "**************************"
echo "UnaryDemo.java"
echo "**************************"
python tmp.py

txl .\java\Website.java -o tmp.py
echo "**************************"
echo "Website.java"
echo "**************************"
python tmp.py

txl .\java\WhileDemo.java -o tmp.py
echo "**************************"
echo "WhileDemo.java"
echo "**************************"
python tmp.py

del tmp.py
