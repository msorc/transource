#!/bin/sh

found=`ls  java`
echo "" > t.bat
for f in ${found}; do 
    echo "txl .\\java\\${f} -o tmp.py" >> t.bat
    echo "echo \"**************************\"" >> t.bat
    echo "echo \"${f}\"" >> t.bat
    echo "echo \"**************************\"" >> t.bat   
    echo "python tmp.py" >> t.bat
    echo "" >> t.bat
done

echo "del tmp.py" >> t.bat