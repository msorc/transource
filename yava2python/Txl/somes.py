def synchronize(f)
    lock = threading.RLock()
    @wraps(f)
    def wrapper(*args, **kw):
        lock.aquire()
        try:
            return f(*args, **kw)
        finally:
            lock.release()
    return wrapper