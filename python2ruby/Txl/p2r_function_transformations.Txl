% python2ruby project
% function transformations
% Mykhaylo Sorochan, 2011
% msorc@bigmir.net
%
% Copyright 2011, Sphere Consulting Inc. and Mykhaylo Sorochan of Sphere Consulting Inc.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%    Redistributions of source code must retain the above copyright notice,
%    this list of conditions and the following disclaimer.
%    Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
%
%    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
%    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
%    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
%    AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
%    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.

%% prepare
function functionTransformations
    replace [program]
        P [program]
    by
        P
          [addEndToOneLineFunc]
          [addEndToFunctionDefinition]

          [lenToSize]
          [multilenToSize]
          [shuffleToShuffle]
          [typeToClass]
          [chrToChr]
          [ordToTrail]
end function


% def fun2 return x --> def fun2; return x; end
rule addEndToOneLineFunc
    replace $ [funcdef]
        'def N [dotted_name] P [opt parameters] I [indentor]
             S [simple_stmt] _ [opt ';] _ [opt 'end] E [endofline]
    deconstruct not I
        ';
    by
        'def N P '; S '; 'end E
end rule

% add end to [funcdef]
rule addEndToFunctionDefinition
    replace $ [funcdef]
        F [funcdef]
    by
        F [addEndToSuite]
end rule

% len(e) --> e.size()
rule lenToSize
    replace [power]
        'len '( Arg1 [argument] ')
    deconstruct Arg1
        A [atom] TS1 [repeat trailer]
    construct Trailers [repeat trailer]
       '.size()
    by
        A TS1 [. Trailers]
end rule

% len(e + e2) --> (e+e2).size()
rule multilenToSize
    replace [power]
        'len '( Test [test] ')
    construct Trailers [repeat trailer]
       '.size()
    construct A [atom]
       '( Test ')
    by
        A Trailers
end rule

% shuffle(e) --> e.shuffle()
rule shuffleToShuffle
    replace [power]
        'shuffle '( Arg1 [argument] ')
    deconstruct Arg1
        A [atom] TS1 [repeat trailer]
    construct Trailers [repeat trailer]
       '.shuffle()
    by
        A TS1 [. Trailers]
end rule

% type(e) --> e.class()
rule typeToClass
    replace [power]
        'type '( Arg1 [argument] ')
    deconstruct Arg1
        A [atom] TS1 [repeat trailer]
    construct Trailers [repeat trailer]
       '.type()
    by
        A TS1 [. Trailers]
end rule

% ord(e) --> e[0]
rule ordToTrail
    replace [power]
        'ord '( Arg1 [argument] ')
    deconstruct Arg1
        A [atom] TS1 [repeat trailer]
    construct Trailers [repeat trailer]
       '[ '0 ']
    by
        A TS1 [. Trailers]
end rule

% chr(e) --> e.chr()
rule chrToChr
    replace [power]
        'chr '( Arg1 [argument] ')
    deconstruct Arg1
        A [atom] TS1 [repeat trailer]
    construct Trailers [repeat trailer]
       '.chr()
    by
        A TS1 [. Trailers]
end rule
