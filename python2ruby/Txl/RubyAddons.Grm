% python2ruby project
% python grammar mixins with Ruby definitions based on the previous work java2tcl and python2tcl
% Mykhaylo Sorochan, Sphere Consulting Inc.
% msorc@bigmir.net

% Python 2.5 grammar used: v1.3 available at www.txl.ca

% Copyright 2010-2011, Sphere Consulting Inc. and Mykhaylo Sorochan of Sphere Consulting Inc.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%    Redistributions of source code must retain the above copyright notice,
%    this list of conditions and the following disclaimer.
%    Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
%
%    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
%    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
%    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
%    AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
%    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.

compounds
    '&& '|| 
    '::
    '=>
    '...
end compounds

keys
    'begin 'end 'do
    'unless 'elsif 'until
    'rescue
    'then
end keys

tokens
    sysargs   "$\d" % $0, $1, ...
end tokens

% some grammar definitions

% packaging
redefine import_stmt
    ...
    |   [ruby_require]
end redefine

define ruby_require
    'require [string] [NL]
end define

% identifiers
redefine dotted_name
    ...
    |   [composite_id]
end redefine

%% indentor
define indentor
    [attr ':] | [opt ';]
end define

%% ruby composite id Module::Name etc.
define composite_id
    [opt id] [repeat composite_id_tail]
end define

define composite_id_tail
    [SPOFF] ':: [id] [SPON]
end define

%% ruby method id
define method_id
    [dotted_name]
    | [SPOFF] [id] [method_suffix] [SPON]
end define

define method_suffix
    '? | '! | '= | '!=
end define

redefine trailer
        [SPOFF] '( [SPON] [IN] [arglist] [repeat endofline] ') [EX]
    |   [SPOFF] '[ [SPON] [IN] [subscriptlist] '] [EX]
    |   [repeat endofline] '. [repeat endofline] [method_id]
end redefine

% class definitions
redefine classdef
    'class [composite_id] [opt superclassdef] [indentor] [suite]
    |   'class [opt id] [SPOFF] '( [SPON] [opt testlist] [repeat endofline] ') ': [suite]
end redefine

define superclassdef
    '< [composite_id]
end define

% method definition redefine
redefine funcdef
    'def [dotted_name] [opt parameters] [indentor] [suite]
end redefine

% power
redefine power
    ... |
    [symbolliteral]
end redefine

redefine comp_op
    ... |
    '...
end define


% block
redefine suite
        [indent] [endofline]
        [repeat stmt_or_newline+]
        [dedent]
    |   [simple_stmt] [opt ';] [opt 'end] [endofline]
    |   'do [opt block_parameters]
        [indent] [endofline]
        [repeat stmt_or_newline+]
        [dedent]
    |   [block]
end redefine

define block
    '{ [opt block_parameters] [simple_stmt] '}
end define

%% ruby block parameters
define block_parameters
    '| [exprlist] '|
end define

%% ruby block end
redefine dedent
    ...
    |   [EX] 'end [NL]
end redefine

% exceptions
redefine try_stmt
    [try_begin] [indentor]
        [suite]
    [repeat except_clause_suite]
    [opt else_clause]
    [opt finally_clause]
end redefine

define try_begin
    'try | 'begin
end define

redefine except_clause_suite
    [except_clause] [indentor]
        [suite]
end redefine

redefine except_clause
    [except_rescue] [opt except_test]
end redefine

define except_rescue
    'except | 'rescue
end define

redefine finally_clause
    [finally_ensure] [indentor]
        [suite]
end redefine

define finally_ensure
    'finally | 'ensure
end define

% if
redefine if_stmt
    'if [test] [indentor]
        [suite]
    [repeat elif_clause]
    [opt else_clause]
end redefine

redefine elif_clause
    [elif_elsif] [test] [indentor]
        [suite]
end redefine

define elif_elsif
    'elif | 'elsif
end define

redefine else_clause
    'else [indentor]
        [suite]
end redefine

% post if
redefine simple_stmt
    [small_cond_stmt] [repeat semicolon_small_stmt] [opt ';] [opt 'end]
    |   [comment]
end redefine

redefine semicolon_small_stmt
    '; [small_cond_stmt]
end redefine

define small_cond_stmt
    [small_stmt] [opt stmt_post_cond]
end define

define stmt_post_cond
    'if [test]
end define

% for
redefine for_stmt
    [iterator_stmt]
end redefine

redefine iterator_stmt
    [iterator_stmt_header]
    [suite]
    [opt else_clause]
end redefine

define iterator_stmt_header
        [for_stmt_header]
    |   [enumerator_stmt_header]
end define

define for_stmt_header
    'for [exprlist] 'in [testlist] [indentor]
end define

% ruby Enumerable each
define enumerator_stmt_header
    [testlist] [SPOFF] '. 'each [SPON] 'do [opt enumerator_local_vars] [endofline]
end define

define enumerator_local_vars
    '| [exprlist] '|
end define

% while
redefine while_stmt
    'while [test] [indentor]
        [suite]
    [opt else_clause]
end redefine

% common loops
define breaking_stmts
        [while_stmt]
    |   [for_stmt]
    |   [try_stmt]
end define

% print statements
redefine print_stmt
        [print_method] [opt '>>] [list test] [opt ',]
end redefine

define print_method
    'print | 'puts
end define

% types
%% hash
redefine dict_entry
    [repeat endofline]
    [test] [dict_hash_delimiter]
    [repeat endofline] % jrc
    [test]
end redefine

%% dict_hash delimiter
define dict_hash_delimiter
    ': | '=>
end define

%% list slicing
redefine subscript
    ...
    |   [opt test] '.. [opt test]
end redefine

% ruby :symbol
define symbolliteral
    ': [SPOFF] [id] [SPON]
end define

redefine factor
    [power]
    |   [repeat unary_op] [SPOFF] [power] [SPON]
end redefine


redefine literal
    ...
    |   [sysargs]
end redefine

% comparison operations
redefine comp_op
    ...
    | '!
end redefine

redefine not_test
    [repeat not_test_prefix] [repeat endofline] [comparison]
end redefine

define not_test_prefix
    'not | '!
end define

redefine orop_or_test
    [repeat endofline] [or_test_op] [repeat endofline] [or_test]
end redefine

define or_test_op
    'or | '||
end define

redefine andop_and_test
    [repeat endofline] [and_test_op] [repeat endofline] [and_test]
end redefine

define and_test_op
    'and | '&&
end define

