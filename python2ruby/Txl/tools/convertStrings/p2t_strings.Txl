% python2tcl project
% string prefix transforms
% Mykhaylo Sorochan, 2009
% msorc@bigmir.net

% Copyright 2009 Mykhaylo Sorochan
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%    Redistributions of source code must retain the above copyright notice,
%    this list of conditions and the following disclaimer.
%    Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
%
%    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
%    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
%    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
%    AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
%    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.

% for shell /bin/sh (Windows?)

include "python.Grm"

%% 
function main
    replace [program]
        P [program]
    by
        P [transformStringPrefix]
          [transformLongChar2Comment]
          [transformLongString2Comment]
          [transformLongChar2String]
          [transformLongString2String]
end function

redefine simple_stmt
        ...
    | [repeat comment_NL]
end redefine

define comment_NL
        [comment] [NL]
end define


rule transformStringPrefix
    replace [stringliteral]
        S [stringliteral]
    deconstruct S
        _ [stringprefix] _ [string]
    construct _tmp1 [stringliteral]
        S [write "in.str"]
    construct ShellCmd [stringlit]
        _ [+ "python convert_unicode.py"]
    construct _tmp [stringlit]
        ShellCmd [system ShellCmd]
    construct P [program]
        _ [read "out.str"]
    deconstruct * [stringlit] P
        Str [stringlit]
    by
        Str
end rule

% transform Charlit to Stringlit
rule transformCharlit2Stringlit
    replace [string]
        Charlit [charlit]
    construct Len [number]
        _ [# Charlit]
    construct Stringlit [stringlit]
        _ [+ Charlit] [: 1 Len]
    by
        Stringlit
end rule

rule transformLongChar2Comment
    replace [simple_stmt]
        S [longcharlit] _ [repeat endofline] 
    construct _tmp1 [stringliteral]
        S [write "in.str"]
    construct ShellCmd [stringlit]
        _ [+ "python convert_long_comment.py"]
    construct _tmp [stringlit]
        ShellCmd [system ShellCmd]
    construct P [program]
        _ [read "out.str"]
    construct Coms [repeat comment]
        _ [^ P]
    construct NComs [repeat comment_NL]
        _ [addNLToComment each Coms]
    by
        NComs
end rule


rule transformLongString2Comment
    replace [simple_stmt]
        S [longstringlit] _ [repeat endofline] 
    construct _tmp1 [stringliteral]
        S [write "in.str"]
    construct ShellCmd [stringlit]
        _ [+ "python convert_long_comment.py"]
    construct _tmp [stringlit]
        ShellCmd [system ShellCmd]
    construct P [program]
        _ [read "out.str"]
    construct Coms [repeat comment]
        _ [^ P]
    construct NComs [repeat comment_NL]
        _ [addNLToComment each Coms]
    by
        NComs
end rule

function addNLToComment Comment [comment]
    replace [repeat comment_NL]
        Coms [repeat comment_NL]
    construct Newline [NL]
        _ 
    construct NewCom [comment_NL]
        Comment Newline
    by
        Coms [. NewCom]
end function


rule transformLongChar2String
    replace [string]
        S [longcharlit]
    construct _tmp1 [stringliteral]
        S [write "in.str"]
    construct ShellCmd [stringlit]
        _ [+ "python convert_long.py"]
    construct _tmp [stringlit]
        ShellCmd [system ShellCmd]
    construct P [program]
        _ [read "out.str"]
    deconstruct * [stringlit] P
        Str [stringlit]
    by
        Str
end rule


rule transformLongString2String
    replace [string]
        S [longstringlit]
    construct _tmp1 [stringliteral]
        S [write "in.str"]
    construct ShellCmd [stringlit]
        _ [+ "python convert_long.py"]
    construct _tmp [stringlit]
        ShellCmd [system ShellCmd]
    construct P [program]
        _ [read "out.str"]
    deconstruct * [stringlit] P
        Str [stringlit]
    by
        Str
end rule
