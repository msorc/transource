a = 10
b = 11

if a < 5:
    print a

if a < 5:
    print "yes"
else:
    print "no" 

if b < 5:
    print "yes"
elif b == 65:
    print "65"

if b < 5:
    print "yes"
elif b == 65:
    print "65"
else:
    print "no"

if b < 5:
    print "yes"
elif b == 65:
    print "65"
elif b == 67:
    print "67"

if b < 8:
    print "yes"
elif b == 6:
    print "6"
elif b == 7:
    print "7"
else:
    print "no"

if __name__ == '__main__':
  print "main if"
