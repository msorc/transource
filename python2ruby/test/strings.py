'''Calls, conferences.
'''


class C(B):
    '''Represents a voice/video call.
    '''

    def Answer(self):
        '''Answers the call.
        '''
        self._Property('STATUS', 'INPROGRESS')

    def CanTransfer(self, Target):
        '''Queries if a call can be transferred to a contact or phone number.

        @param Target: Skypename or phone number the call is to be transfered to.
        @type Target: unicode
        @return: True if call can be transfered, False otherwise.
        @rtype: bool
        '''
        return self._Property('CAN_TRANSFER %s' % Target) == 'TRUE'

    ConferenceId = property(_GetConferenceId,
    doc='''ConferenceId.

    @type: int
    ''')

