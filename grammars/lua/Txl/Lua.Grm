% TXL Lua 5.1 Grammar
% Copyright 2013 Mykhaylo Sorochan
% msorc@yandex.ru

% Adapted from The Complete Syntax of Lua available at:
% http://www.lua.org/manual/5.1/manual.html#8

comments
  --
end comments

keys
  'and 'break 'do 'else 'elseif 'end 'false 'for 'function 'if 'in 'local 'nil 'not 'or 'repeat 'return 'then 'true 'while
end keys

compounds
  <= >= == ~= .. ...
end compounds

define program
  [chunk]
end define

define chunk
  [repeat chunk_statement] [opt chunk_last_statement]
end define

define chunk_last_statement
  [last_statement] [NL]
end define

define chunk_statement
  [statement] [opt sep] [NL]
end define

define block
  [NL][IN] [chunk] [EX]
end define

define sep
  ';
end define

define field_sep
  ', | ';
end define

define statement
  [variables_assignment]
    | [function_call] 
    | [do_statement]
    | [while_statement]
    | [loop_repeat_statement]
    | [if_statement]
    | [for_assign_statement]
    | [for_in_statement]
    | [function_statement]
    | [local_function_declaration]
    | [local_name_list_declaration]
end define

define if_statement
  'if [expression] 'then [block] [repeat elseif_clause] [opt else_clause] 'end [opt sep]
end define

define for_assign_statement
  'for [id] '= [expression] ', [list expression+] 'do [block] 'end [opt sep]
end define

define for_in_statement
  'for [name_list] in [expression_list] 'do [block] 'end [opt sep]
end define

define function_statement
  'function [function_name] [function_body]
end define

define local_function_declaration
  'local 'function [id] [function_body] [opt sep]
end define

define local_name_list_declaration
  'local [name_list] [opt expression_list_clause] [opt sep]
end define

define elseif_clause
  'elseif [expression] 'then [block]
end define

define else_clause
  'else [block]
end define

define expression_list_clause
  '= [expression_list]
end define

define last_statement
  [return_statement] | [break_statement]
end define

define return_statement
  'return [opt expression_list] [opt sep]
end define

define break_statement
  'break [opt sep]
end define

define variables_assignment
  [variables_list] '= [expression_list] [opt sep]
end define

define variables_list
  [list variable+]
end define

define variable
  [id] | [prefix_expression] [SPOFF] '[ [SPON] [expression] '] | [prefix_expression] [dot_prefix_name]
end define

define expression_list
  [list expression+]
end define

define expression
    [primary_expression]
    | [operational_expression]
end define

define primary_expression
  'nil
    | 'false
    | 'true
    | [number]
    | [stringlit]
    | [charlit]
    | '...
    | [anonymous_function_statement]
    | [table_constructor]
    | [prefix_expression]
end define

define operational_expression
  [and_expression] [repeat or_and_expression]
end define

define or_and_expression
  'or [and_expression]
end define

define and_expression
  [comparison_expression] [repeat and_comparison_expression]
end define

define and_comparison_expression
  'and [comparison_expression]
end define

define comparison_expression
  [concat_expression] [repeat comparison_concat_expression]
end define

define comparison_concat_expression
  [relativity_operation] [concat_expression]
  | [equality_operation] [concat_expression]        
end define

define concat_expression
  [add_expression] [repeat concat_add_expression]
end define

define concat_add_expression
  '.. [add_expression]
end define

define add_expression
  [multiply_expression] [repeat add_multiply_expression]
end define

define add_multiply_expression
  [additive_operation] [multiply_expression]
end define

define multiply_expression
  [unary_expression] [repeat multiply_unary_expression]
end define

define multiply_unary_expression
  [multiplicative_operation] [unary_expression]
end define

define unary_expression
  [primary_expression] | [opt unary_operation] [not_expression]
end define

define not_expression
  '^ [expression]
end define

define prefix_expression
  [variable]
  | [function_call]
  | '( [expression] ')
end define

define function_name
  [id] [repeat dot_prefix_name] [opt colon_prefix_name]
end define

define dot_prefix_name
  '. [id]
end define

define colon_prefix_name
  ': [id]
end define

% define prefix_name
%   [prefix_expression] [prefix_name_suffix]
% end define

define prefix_name_suffix
  [dot_prefix_name] | [colon_prefix_name]
end define

define name_list
  [list id+]
end define

define do_statement
  'do [block] 'end [opt sep]
end define

define while_statement
  'while [expression] 'do [block] 'end [opt sep]
end define

define loop_repeat_statement
  'repeat [block] 'until [expression] [opt sep]
end define

define function_call
    [prefix_expression] [opt colon_prefix_name] [arguments] [opt sep]
end define

define arguments
  [SPOFF] '( [SPON] [opt expression_list] ')
    | [table_constructor]
    | [stringlit]
    | [charlit]
end define

define anonymous_function_statement
  'function [function_body]
end define

define function_body
  '( [opt parameters_list] ') [block] 'end
end define

define parameters_list
  [name_list] [opt parameters_list_rest]
    | [parameters_rest]
end define

define parameters_list_rest
  ', '...
end define

define parameters_rest
  '...
end define

define field_list
  [field] [repeat field_list_part] [opt field_sep]
end define

define table_constructor
  '{ [opt field_list] '}
end define

define field_list_part
  [field_sep] [field]
end define

define field
  '[ [expression] '] '= [expression]
   | [id] '= [expression] 
   | [expression]
end define

define unary_operation
  '- | 'not | '#
end define

define relativity_operation
  '< | '> | '<= | '>=
end define

define additive_operation
  '+ | '-
end define

define multiplicative_operation
  '*  | '/ | '%
end define

define equality_operation
  '~= | '==
end define
